package com.focaplo.sports.pub.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.focaplo.sports.pub.utils.DateUtil;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetTodaySchedule  extends HttpServlet{
	Logger log = Logger.getLogger(this.getClass());
	public MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		DateTime today = DateUtil.getNow();
		DateTime _4hoursAgo = today.minusHours(4);
		log.debug("getting current scoreboard of " + _4hoursAgo);
		Map<String, String> currentSchedule = (Map<String, String>)cache.get("today");
		
		String response = null;
		try {
			JSONObject obj = new JSONObject();
			obj.put("schedule", currentSchedule);
			obj.put("date", _4hoursAgo.toString("yyyyMMdd"));
			response = obj.toString();
		} catch (Exception e) {
			log.error("ERROR", e);
			response="{\"status\":\"fail\"}";
		} 
		
		resp.setContentType("application/json");
		PrintWriter pw = resp.getWriter();
		pw.println(response);
		pw.flush();
	}
}
