package com.focaplo.sports.pub.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.focaplo.sports.pub.nba.dao.NbaDao;
import com.focaplo.sports.pub.utils.DateUtil;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetCurrentScoreBoardServlet extends HttpServlet{
	Logger log = Logger.getLogger(this.getClass());
	public MemcacheService cache = MemcacheServiceFactory.getMemcacheService();
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String date = req.getParameter("date");
		if(date==null){
			DateTime today = DateUtil.getNow();
			DateTime _4hoursAgo = today.minusHours(4);
			//log.debug("getting current scoreboard of " + _4hoursAgo);
			date = _4hoursAgo.toString("yyyyMMdd");
		}
		NbaDao dao = new NbaDao();
		
		String response = null;
		try {
			JSONObject obj = dao.getLastScoresOfDate(date);
			obj.put("date", date);
			if(!obj.has("games") || obj.getJSONArray("games").length()==0){
				Map<String, String> todaySchedule = (Map<String, String>)cache.get("today");
				if(todaySchedule==null){
					obj.put("message", "can not find the today schedule in memcache.");
				}else{
					String firstGameStartTimestamp =  ((Map<String, String>)cache.get("today")).get("firstGameStartTimestamp");
					if(firstGameStartTimestamp==null){
						obj.put("message", "No first game start time found, is it off season or no game today?");
					}else{
						obj.put("description", "No game score found, is it before today game starts at " + DateUtil.printTime(Long.parseLong(firstGameStartTimestamp)) + " ?");
					}
					
				}
				//obj.put("schedule",  (Map<String, String>)cache.get("today"));
			}
			response = obj.toString();
		} catch (Exception e) {
			log.error("ERROR", e);
			response="{\"status\":\"fail\"}";
		} 
		
		resp.setContentType("application/json");
		PrintWriter pw = resp.getWriter();
		pw.println(response);
		pw.flush();
	}

}
